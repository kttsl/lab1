//2. Implement your code to turn on the LED for 3 seconds and then turn off the LED for 1 seconds

#define LED_PIN 16
void setup() {
  pinMode(LED_PIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_PIN, HIGH);
  delay(3000);  
  digitalWrite(LED_PIN, LOW);
  delay(1000);
}
