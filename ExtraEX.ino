#define INIT 0
#define DOOR_OPEN 1
#define DOOR_CLOSE 2
#define SENSOR_INPUT 0
#define DOOR_PIN LED_BUILTIN

uint64_t timer;
int state_idx, state_sensor;

void setup() {
  Serial.begin(9600);
  pinMode(SENSOR_INPUT, INPUT_PULLUP);
  pinMode(DOOR_PIN, OUTPUT);  
  state_idx = INIT;
  state_sensor = 0;
}

void loop() {
  state_sensor = digitalRead(SENSOR_INPUT);
  timer += 10;
  switch(state_idx){
    case INIT:
      timer = 0;
      digitalWrite(DOOR_PIN, HIGH);
      state_idx = DOOR_CLOSE;
      break;   
    case DOOR_OPEN:
      if (state_sensor == 0){ // co' nguoi => reset timer
        timer = 0;
      }
      else if (state_sensor == 1 && timer >= 3000){ // ko co nguoi > 3s => close
          state_idx = DOOR_CLOSE;
          digitalWrite(DOOR_PIN, HIGH); // HIGH to turn off LED
          timer = 0;
      }
      break;

    case DOOR_CLOSE:
      if (state_sensor == 0){ // co' nguoi`
        timer = 0;
        state_idx = DOOR_OPEN;
        digitalWrite(DOOR_PIN, LOW); // LOW to turn on LED
      }
      break;
      
    default:
      break;
  }
  delay(10);
}
