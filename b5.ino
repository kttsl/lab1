#define INIT    0
#define LED_ON  1
#define LED_OFF 2
#define LED_PIN LED_BUILTIN

int T_on, T_off;

void setup() {
  pinMode(LED_PIN, OUTPUT);

  T_on = 300;
  T_off = 500;
}
long timer = 0, state_idx = INIT;

void loop() {
  timer += 10;
  switch (state_idx){
    case INIT:
      timer = 0;
      digitalWrite(LED_PIN, LOW);
      state_idx = LED_ON;
      break;
    case LED_ON:
      if(timer >= T_on){
        timer = 0;
        state_idx = LED_OFF;
        digitalWrite(LED_PIN, HIGH); // pin HIGH to turn off LED_BUILTIN
      }
      break;
    case LED_OFF: 
      if(timer >= T_off){
        timer = 0;
        state_idx = LED_ON;
        digitalWrite(LED_PIN, LOW); // pin LOW to turn on LED_BUILTIN
      }
      break;
    default:
      break;
  }

  delay(10);
}
