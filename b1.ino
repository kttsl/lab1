//1. Implement your code to blink the LED every 2 seconds.


#define LED_PIN 2
void setup() {
  pinMode(LED_PIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_PIN, HIGH);
  delay(2000);  
  digitalWrite(LED_PIN, LOW);
  delay(2000);
}
